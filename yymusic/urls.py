from django.urls import path
from web.utils import SpiderFile

urlpatterns = [
    path('yymusic/api/search/', SpiderFile.Api_Search),  # 搜索音乐
    path('yymusic/api/mp3/', SpiderFile.Api_GetMusicInfo),  # 获取MP3音乐
    path('yymusic/api/mv/', SpiderFile.Api_GetMusicMv),  # 获取mv音乐
]
